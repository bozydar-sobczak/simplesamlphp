<?php
/**
 * SAML 2.0 remote SP metadata for SimpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote
 */

/*
 * Example SimpleSAMLphp SAML 2.0 SP
 */
$metadata['http://localhost:8000/simplesaml/module.php/saml/sp/metadata.php/default-sp'] = array(
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'simplesaml.nameidattribute' => 'email',
	'AssertionConsumerService' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',
	'SingleLogoutService' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp',
);

$metadata['http://localhost:3000/users/saml/metadata'] = array(
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'simplesaml.nameidattribute' => 'email',
	'AssertionConsumerService' => 'http://localhost:3000/users/saml/auth',
	'SingleLogoutService' => 'http://localhost:3000/users/saml/idp_sign_out',
);

$metadata['http://localhost:5000/users/saml/metadata'] = array(
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'simplesaml.nameidattribute' => 'email',
    'AssertionConsumerService' => 'http://localhost:5000/users/saml/auth',
    'SingleLogoutService' => 'http://localhost:5000/users/saml/idp_sign_out',
);

$metadata['http://localhost:5000/users/auth/idp1/saml/metadata'] = array (
  'entityid' => 'http://localhost:5000/users/auth/idp1/saml/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://localhost:5000/users/auth/idp1/saml/callback',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:5000/users/auth/idp1/saml/slo',
      'ResponseLocation' => 'http://localhost:5000/users/auth/idp1/saml/slo',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'attributes' => 
  array (
    0 => 'email',
    1 => 'name',
    2 => 'first_name',
    3 => 'last_name',
  ),
  'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
  'name' => 
  array (
    'en' => 'Required attributes',
  ),
  'description' => 
  array (
  ),
  'validate.authnrequest' => false,
  'saml20.sign.assertion' => false,
);

$metadata['http://localhost:5000/users/auth/multisaml/idp1/metadata'] = array (
  'entityid' => 'http://localhost:5000/users/auth/multisaml/idp1/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://localhost:5000/users/auth/multisaml/idp1/callback',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:5000/users/auth/multisaml/idp1/slo',
      'ResponseLocation' => 'http://localhost:5000/users/auth/multisaml/idp1/slo',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'attributes' => 
  array (
    0 => 'email',
    1 => 'name',
    2 => 'first_name',
    3 => 'last_name',
  ),
  'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
  'name' => 
  array (
    'en' => 'Required attributes',
  ),
  'description' => 
  array (
  ),
  'validate.authnrequest' => false,
  'saml20.sign.assertion' => false,
);

$metadata['http://localhost:5000/users/auth/idp2/saml/metadata'] = array (
  'entityid' => 'http://localhost:5000/users/auth/idp2/saml/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://localhost:5000/users/auth/idp2/saml/callback',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:5000/users/auth/idp2/saml/slo',
      'ResponseLocation' => 'http://localhost:5000/users/auth/idp2/saml/slo',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'attributes' => 
  array (
    0 => 'email',
    1 => 'name',
    2 => 'first_name',
    3 => 'last_name',
  ),
  'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
  'name' => 
  array (
    'en' => 'Required attributes',
  ),
  'description' => 
  array (
  ),
  'validate.authnrequest' => false,
  'saml20.sign.assertion' => false,
);

$metadata['http://localhost:5000/users/auth/multisaml/idp2/metadata'] = array (
  'entityid' => 'http://localhost:5000/users/auth/multisaml/idp2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://localhost:5000/users/auth/multisaml/idp2/callback',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:5000/users/auth/multisaml/idp2/slo',
      'ResponseLocation' => 'http://localhost:5000/users/auth/multisaml/idp2/slo',
    ),
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'attributes' => 
  array (
    0 => 'email',
    1 => 'name',
    2 => 'first_name',
    3 => 'last_name',
  ),
  'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
  'name' => 
  array (
    'en' => 'Required attributes',
  ),
  'description' => 
  array (
  ),
  'validate.authnrequest' => false,
  'saml20.sign.assertion' => false,
);

$metadata['http://localhost:5000/users/auth/multisaml/594aff98e1382353a8c5f8d5/metadata'] = array (
  'entityid' => 'http://localhost:5000/users/auth/multisaml/594aff98e1382353a8c5f8d5/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-sp-remote',
  'AssertionConsumerService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'http://localhost:5000/users/auth/multisaml/594aff98e1382353a8c5f8d5/callback',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'SingleLogoutService' => 
  array (
  ),
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  'attributes' => 
  array (
    0 => 'email',
    1 => 'name',
    2 => 'first_name',
    3 => 'last_name',
  ),
  'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
  'name' => 
  array (
    'en' => 'Required attributes',
  ),
  'description' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDVzCCAj+gAwIBAgIJAKtsOgt2vX8YMA0GCSqGSIb3DQEBCwUAMEIxCzAJBgNVBAYTAlhYMRUwEwYDVQQHDAxEZWZhdWx0IENpdHkxHDAaBgNVBAoME0RlZmF1bHQgQ29tcGFueSBMdGQwHhcNMTcwNzA1MjEzMTE0WhcNMjcwNzA1MjEzMTE0WjBCMQswCQYDVQQGEwJYWDEVMBMGA1UEBwwMRGVmYXVsdCBDaXR5MRwwGgYDVQQKDBNEZWZhdWx0IENvbXBhbnkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsg9ISNjGtrxHGXnUPYSsc4LoXFSqsrnT1eKotOcwInX8N+W32MKvSf/ecDcN7LrdywBPNt/YeWtMvuUoqMVIkwF1aj+5bFsOt2rFN7mcM6Nu7jWcDVjlWRk16Amum0u0OGc7nZHwL+I9TY31lKGUUc4Gp3I6NI6knSvgDeVRQA/+QS/CschmxCWxJu6jsGYvNFAFnMoh85brpKgtIkOoSyRekRudd4ZYjTHbR2JncQPf1YuY6+RpX1TxtxGFPKhizITlrmMmKuR1ITyX+pBEYZ3PZNthcC5PyFgXPbqhkHjfrHlqldK72lzYtonXQrJoAt+oaGmPsmT1P2LCWd7yRQIDAQABo1AwTjAdBgNVHQ4EFgQUZqXHJ6kwHYtjTg3nifUcXkYrzyIwHwYDVR0jBBgwFoAUZqXHJ6kwHYtjTg3nifUcXkYrzyIwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAQSPgDbjdayfRpN4aG7jcTbs3GDB4CvbcHALrh7FC46wxlJtI+pd8dM80KDOFzvhwRNQw9/ct3/nX2k8RUmsi4HKG6KQTyS1mWnJ7Fc5CxvYMUUSuAOWNNjbAsN2az7xOaG+drCOW+7Wv8LVdP0L/Iyxvy0X9QhprbXNxtul9YZMNsZV0W6YXwRDN3quWCnczo7KN3wr9AFXjefGV7XT1Vl1o2keML28lnUGnUJ3v3bH17a42M8PBdbrhc1pSXqPGarXIYliLbkVzjEfgLBPALdOlWdtO16crWwuYj0nhikB7CQKn4MRdwNbywympGI7HTBQiCxsXlV0q6Jz7V1MFjg==
',
    ),
  ),
  'validate.authnrequest' => false,
  'saml20.sign.assertion' => true,
);

/*
 * This example shows an example config that works with Google Apps for education.
 * What is important is that you have an attribute in your IdP that maps to the local part of the email address
 * at Google Apps. In example, if your google account is foo.com, and you have a user that has an email john@foo.com, then you
 * must set the simplesaml.nameidattribute to be the name of an attribute that for this user has the value of 'john'.
 */
$metadata['google.com'] = array(
	'AssertionConsumerService' => 'https://www.google.com/a/g.feide.no/acs',
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
	'simplesaml.nameidattribute' => 'uid',
	'simplesaml.attributes' => FALSE,
);
