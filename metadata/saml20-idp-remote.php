<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */
$metadata['http://localhost:8000/simplesaml'] = array(
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'simplesaml.nameidattribute' => 'email',
    'SingleSignOnService'  => 'http://localhost:8000/simplesaml/saml2/idp/SSOService.php',
    'SingleLogoutService'  => 'https://localhost:8000/simplesaml/saml2/idp/SingleLogoutService.php',
    'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb',
    'sign.authnrequest'    => true
);



